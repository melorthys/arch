# Arch Linux

- Get the image and pgp signature from https://archlinux.org/download/
- Verify the image:
```
gpg or md5sum
```

- Set keyboard layout


- Verify the boot mode if its UEFI or BIOS.
```
ls /sys/firmware/efi/efivars
```

- Connect the internet
```
ip link
ping archlinux.com
```

- Update the system clock:
```
timedatectl set-ntp true
```

### Disk Partition
- Find the device:
```
fdisk -l
```

- Erase the disk:
```
shred --verbose --random-source=/dev/urandom --iterations=3 /dev/sda
```

- Start partitioning:
```
cfdisk /dev/sda
```

bios> dos > 100M bootable Linux + 1G swap + rest /
uefi> gpt >

- Check partitioning:
```
fdisk -l
``` 

- Encrypt root partition:
```
cryptsetup --verbose --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat /dev/sda3
```

- Unlock the partition (root will be the device mapper name):
```
cryptsetup open --type luks /dev/sda3 root
```

- Format partitions:
```
mkfs.ext4 /dev/sda1
mkswap /dev/sda2
mkfs.ext4 /dev/mapper/root
```

- Mount the root file system:
```
mount /dev/mapper/root /mnt

```

- Mount the boot partition:
```
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
```

- Enable swap partition:
```
swapon /dev/sda2
```
 

## Installation
- Select mirrors:
```
```

- Install essential packages:
```
pacstrap /mnt base linux linux-firmware
```

## Configure the system
- Generate an fstab file:
```
genfstab -U /mnt >> /mnt/etc/fstab
```

- Change root into the new system:
```
arch-chroot /mnt
```

### Timezone
- Set the timezone:
```
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```

- Run hwclock(8) to generate /etc/adjtime:
```
hwclock --systohc
```

### Localization 
- Edit /etc/locale.gen and uncomment "en_US.UTF-8 UTF-8" and other needed locales. Generate the locales by running: 
```
locale-gen
```

- Create the /etc/locale.conf file, and set the LANG variable accordingly:
```
LANG=en_US.UTF-8
```

### Network Configuration
- Create the hostname file:
```
cat > /etc/hostname << EOF
myhostname
EOF
```

- Add matching entries to hosts(5):
```
cat >> /etc/hosts << EOF
127.0.0.1	localhost
::1		localhost
127.0.1.1	myhostname.localdomain	myhostname
EOF
```

NOTE: If the system has a permanent IP address, it should be used instead of 127.0.1.1. 

### Users
- Set the root password:
```
passwd
```

- Add a user:
```
useradd -m -g users -G wheel,games,power,optical,storage,scanner,lp,audio,video -s /bin/bash username
```

- Change new user's password:
```
passwd username
```

- Install opendoas
```
pacman -S doas
```

- Give root privileges to the wheel group:
```
cat > /etc/doas.conf << EOF
permit keepenv :wheel
EOF
```

### Boot Loader
- Install boot loader:
```
pacman -S grub-bios os-prober
```

- Add the following kernel parameter to be able to unlock your LUKS encrypted root partition during system startup:
```
GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda3:root"  >> /etc/default/grub
```

- Add encrypt hook:
```
HOOKS="...... encrypt ...." >> /etc/mkinitcpio.conf
```

- Since we added new hook in the mkinitcpio configuration file, we should re-generate our initrams image (ramdisk):
```
mkinitcpio -p linux or mkinitcpio -P ???
```

- Install grub and save it's configuration file:
```
grub-install --recheck /dev/sda
grub-mkconfig --output /boot/grub/grub.cfg
```

- Exit from chroot, unmount the partitions, close the device and reboot (remove the installation media):
```
exit
umount -R /mnt/boot
umount -R /mnt
cryptsetup close root
systemctl reboot
```

### First Boot
- Start dhcp
```
doas systemctl start systemd-networkd
```
- Enable ...:
```
doas systemctl enable systemd-networkd
```

- Find the network interface:
```
ip a
```

- Configure dhcp
```
doas bash -c 'cat > /etc/systemd/network/20-wired.network' << EOF
[Match]
Name=enp1s0

[Network]
DHCP=yes
EOF
```

- Restart network:
```
doas systemctl restart systemd-networkd
```

- Resolve issues, check later:
```
doas bash -c 'cat >> /etc/resolv.conf' << EOF
nameserver 192.168.1.1
EOF
```

### Firewall
- Install firewalld
```
doas pacman -S firewalld
doas systemctl start firewalld
doas systemctl enable firewalld
doas firewall-cmd --permanent --remove-service dhcpv6-client
doas firewall-cmd --permanent --remove-service ssh
doas firewall-cmd --permanent --add-port 22/tcp
doas firewall-cmd --reload
doas firewall-cmd --list-all
```

---

### Troubeshooting with chroot from removable media
```
cryptsetup open --type luks /dev/sda2 root
mount -t ext4 /dev/mapper/root /mnt
mount -t ext4 /dev/sda1 /mnt/boot
arch-chroot /mnt
```

- To unmount:
```
umount -R /mnt/boot
umount -R /mnt
cryptsetup close root
```





