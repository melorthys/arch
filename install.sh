#!/bin/bash


# xinit (https://wiki.archlinux.org/index.php/Xinit)
# --------------------------------------------------

sudo pacman --noconfirm -S xorg-xinit

# copy default xinitrc to $HOME
sudo cp /etc/X11/xinit/xinitrc ~/.xinitrc

#
printf "nitrogen --restore &\npicom &\nexec i3" | tee -a .xinitrc 1>/dev/null 2>error.log

printf "#!/bin/sh\nexec /usr/bin/Xorg -nolisten tcp "$@" vt$XDG_VTNR" | tee ~/.xserverrc 1>/dev/null 2>error.log


